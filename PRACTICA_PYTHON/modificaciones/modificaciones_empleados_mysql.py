# -*- coding: utf-8 -*-

import pymysql

class Conexion:
	def __init__(self, host, puerto, usuario, password, basededatos):
		self.connection = pymysql.connect(
		host=host,
		port=puerto,   
		user=usuario,
		password=password,
		db = basededatos)

		self.cursor = self.connection.cursor()

Conexion1 = Conexion("localhost", 3306, "root", "Itep", "registros")

#Modificaciones: Lectura de un fichero que contiene los empleados a modificar y sus nuevos datos, generar un fichero con las
#modificaciones realizadas y otro con las fallidas.
#El fichero de entrada no contendrá errores de formato y todos los campos vendrán implementados correctamente
#Las altas fallidas se producirán al intentar dar de alta en la tabla, un código de empleado ya existente.
#El fichero no contendrá errores de formato y el campo código de empleado vendrá implementado correctamente.
#Las bajas fallidas se producirán cuando el código de empleado a dar de baja no exista en la tabla.
#El fichero no contendrá errores de formato, todos los campos vendrán implementados correctamente y su formato será igual que el de
#las altas.
#Las modificaciones fallidas se producirán cuando el código de empleado cuyos datos se van a modificar, no existan en la tabla

fichModificar = open("modificaciones_mysql.txt", 'r', encoding="utf8")
fichModificarCorrectas = open("modificaciones_correctas_mysql.txt", 'a', encoding="utf8")
fichModificarErroneas = open("modificaciones_erroneas_mysql.txt", 'a', encoding="utf8")

dato = fichModificar.readline()
while dato != "":
	datos = dato[:-1].split(";")
	sql = "update mae_empleados set codigo='"+datos[0]+"',nombre ='"+datos[1]+"', apellidos = '"+datos[2]+"',nif = '"+datos[3]+"', departamento = '"+datos[4]+"',num_hijos = "+datos[5]+" where codigo = '"+datos[0]+"';"
	Conexion1.cursor.execute(sql)
	Conexion1.connection.commit()
	if  Conexion1.cursor.rowcount > 0:
		fichModificarCorrectas.writelines(dato)
	else:
		fichModificarErroneas.writelines(dato)
	dato = fichModificar.readline()