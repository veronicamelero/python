# -*- coding: utf-8 -*-

import pymysql

# 1 - Conexión a base de datos
# Creamos una clase Conexión, para trabajar con ella
# Parámetros, IP, puerto, usuario, password, nombre de la base de datos

class Conexion:
	def __init__(self, host, puerto, usuario, password, basededatos):
		self.connection = pymysql.connect(
		host=host,
		port=puerto,   
		user=usuario,
		password=password,
		db = basededatos)

		self.cursor = self.connection.cursor()

#podemos eliminar de la clase y la llamada el atributo -port, y la conexión se haría por omisión contra el puerto 3306, trabajariamos contra MySQL

Conexion1 = Conexion("localhost", 3306, "root", "Itep", "registros")

sql = "SELECT * FROM mae_empleados"

fichConsultas = open("consulta_mysql.txt", 'a', encoding="utf8")

Conexion1.cursor.execute(sql)
Empleados = Conexion1.cursor.fetchall()

for Empleados in Empleados:
	fichConsultas.writelines("Código -> "+Empleados[0]+" Nombre -> "+Empleados[1]+" Apellidos -> "+Empleados[2]+" Nif -> "+Empleados[3]+" Departamento -> "+Empleados[4]+" Numero de hijos -> "+str(Empleados[5])+"\n")

fichConsultas.close()
Conexion1.cursor.close()
Conexion1.connection.close()