# -*- coding: utf-8 -*-

import pymysql

# 1 - Conexión a base de datos
# Creamos una clase Conexión, para trabajar con ella
# Parámetros, IP, puerto, usuario, password, nombre de la base de datos

class Conexion:
	def __init__(self, host, puerto, usuario, password, basededatos):
		self.connection = pymysql.connect(
		host=host,
		port=puerto,   
		user=usuario,
		password=password,
		db = basededatos)

		self.cursor = self.connection.cursor()

#podemos eliminar de la clase y la llamada el atributo -port, y la conexión se haría por omisión contra el puerto 3306, trabajariamos contra MySQL

Conexion1 = Conexion("localhost", 3306, "root", "Itep", "registros")

sql = "CREATE TABLE IF NOT EXISTS mae_empleados (codigo VARCHAR(4) primary key, nombre VARCHAR(30) not null," \
"apellidos VARCHAR (45) not null, nif VARCHAR(9) not null, departamento VARCHAR(15) not null, num_hijos INTEGER not null)"
Conexion1.cursor.execute(sql)

# -------- ALTAS -------------
fichAltas = open("altas_mysql.txt", 'r', encoding="utf8")
fichAltasCorrectas = open("altas_correctas_mysql.txt", 'a', encoding="utf8")
fichAltasErroneas = open("altas_erroneas_mysql.txt", 'a', encoding="utf8")

dato = fichAltas.readline()
while dato != "":
	datos = dato[:-1].split(";")
	sql = "insert into mae_empleados (codigo, nombre, apellidos, nif, departamento, num_hijos) values ('"+datos[0]+"','"+datos[1]+"','"+datos[2]+"','"+datos[3]+"','"+datos[4]+"',"+datos[5]+")"
	try:
		Conexion1.cursor.execute(sql)
		Conexion1.connection.commit()
		fichAltasCorrectas.writelines(dato)
	except:
		fichAltasErroneas.writelines(dato)
	dato = fichAltas.readline()

fichAltas.close()
fichAltasCorrectas.close()
fichAltasErroneas.close()


Conexion1.connection.close()