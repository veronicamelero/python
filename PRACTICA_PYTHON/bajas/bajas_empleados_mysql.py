# -*- coding: utf-8 -*-

import pymysql

class Conexion:
	def __init__(self, host, puerto, usuario, password, basededatos):
		self.connection = pymysql.connect(
		host=host,
		port=puerto,   
		user=usuario,
		password=password,
		db = basededatos)

		self.cursor = self.connection.cursor()

Conexion1 = Conexion("localhost", 3306, "root", "Itep", "registros")

fichBajas = open("bajas_mysql.txt", 'r', encoding="utf8")
fichBajasCorrectas = open("bajas_correctas_mysql.txt", 'a', encoding="utf8")
fichBajasErroneas = open("bajas_erroneas_mysql.txt", 'a', encoding="utf8")

dato = fichBajas.readline()
while dato != "":
	datos = dato[:-1]
	sql = "delete from mae_empleados where codigo = ('"+datos+"');"
	Conexion1.cursor.execute(sql)
	Conexion1.connection.commit()
	if  Conexion1.cursor.rowcount > 0:
		fichBajasCorrectas.writelines(dato)
	else:
		fichBajasErroneas.writelines(dato)
	dato = fichBajas.readline()

fichBajas.close()
fichBajasCorrectas.close()
fichBajasErroneas.close()


